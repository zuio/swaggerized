package com.swagger.service;

import com.swagger.model.Task;
import com.swagger.model.Workflow;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFacade<T> {

    private final Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public List<T> findAll() {
        if (entityClass.getName().equals(Task.class.getName())) {
            return (List<T>) getAllTasks();
        }
        return (List<T>) getAllWorkflows();
    }

    public List<Task> getAllTasks() {
        List<Task> list = new ArrayList<>();
        Task task = new Task();
        task.setId(1L);
        task.setName("get coffee");
        list.add(task);
        return list;
    }

    public List<Workflow> getAllWorkflows() {
        List<Workflow> list = new ArrayList<>();
        Workflow workflow = new Workflow();
        workflow.setId(2L);
        workflow.setName("start work");
        list.add(workflow);
        return list;
    }

}
