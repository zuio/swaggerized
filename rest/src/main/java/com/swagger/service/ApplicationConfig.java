package com.swagger.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * CORS filter only works when @ApplicationPath is not empty. @ApplicationPath("") NOT WORKING
*/
@ApplicationPath("rest")
public class ApplicationConfig extends Application {
    
}
