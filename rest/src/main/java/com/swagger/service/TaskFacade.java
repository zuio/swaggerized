package com.swagger.service;

import com.swagger.model.Task;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("task")
public class TaskFacade extends AbstractFacade<Task> {

    public TaskFacade() {
        super(Task.class);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Task> findAll() {
        return super.findAll();
    }

}
