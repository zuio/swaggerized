package com.swagger.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * CORS filter only works when @ApplicationPath is not empty. @ApplicationPath("") NOT WORKING
 * this example serves the swagger.json as resource via GET endpoint, see SwaggerFacade, CORS filter needed
*/
@ApplicationPath("")
public class ApplicationConfig extends Application {
    
}
