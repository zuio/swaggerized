package com.swagger.service;

import com.swagger.model.Workflow;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("workflow")
public class WorkflowFacade extends AbstractFacade<Workflow> {

    public WorkflowFacade() {
        super(Workflow.class);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Workflow> findAll() {
        return super.findAll();
    }

}
