package com.swagger.service;

import java.io.InputStream;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Stateless
@Path("swagger")
public class SwaggerFacade {

    @Inject
    ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSwaggerJson() {
        final InputStream resource = context.getResourceAsStream("/swagger/swagger.json");

        return null == resource
                ? Response.status(NOT_FOUND).build()
                : Response.ok().entity(resource).build();
    }

}
